#pragma once

#include "common_c.h"
#include "genericException.h"

#include <string>
#include <exception>
#include <map>
#include <iterator>
#include <iostream>
#include <cstdlib>

#ifndef __GXX_EXPERIMENTAL_CXX0X__
// to help IDEs parse C++11 headers
#define __GXX_EXPERIMENTAL_CXX0X__
#endif

