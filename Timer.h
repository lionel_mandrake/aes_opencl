/** Copyright (c) 2014 drs.mandrake@gmail.com
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
* This file is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*/

#pragma once

#include<sys/time.h>
#include <unistd.h>

class Timer
{

private:

#ifdef _WIN32
    LARGE_INTEGER liStart;
    LARGE_INTEGER liStop;
    LARGE_INTEGER freq;
    bool m_timeMeasurement;
#endif
#ifdef __linux__
    struct timeval m_timeStart;
    struct timeval m_timeEnd;
#endif
    bool m_isStarted;
    double m_totalTime;

public:
    // creates the timer. the bool arguments tells whether the timer should be started or not.
    Timer(bool);
    // stops the timer and returns the elapsed time, in seconds
    double stop();
    void restart();
    double elapsedTime();
    double totalTime() const
    {
      return m_totalTime;
    }
    void reset()
    {
      m_totalTime = 0;
    }
};

