/***
* OpenCL kernel for AES encryption
*
* Copyright (c) 2014 drs.mandrake@gmail.com
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
* This file is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*/

#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable

__kernel void AESEncrypt(
__global  uchar16  * input,
__global  uchar16  * output,
__global  uchar16  * roundKey,
__global  uchar    * SBox,
__global  uchar4   * mixCol,
__local   uchar    SBoxB[256],
__local   uchar4    mixColB[1024],
__local   uchar16   roundKeyB[11],
const     uint     rounds,
const 	  uint     ioOffset,
const uint blocksPerThread)
{
  const unsigned int localSize = get_local_size(0);
	const unsigned int threadIdx =  get_group_id(0) * localSize * blocksPerThread + get_local_id(0);
  event_t copies[3];
  async_work_group_copy(SBoxB, SBox, (size_t)256, copies[0]);
  async_work_group_copy(mixColB, mixCol, (size_t)1024, copies[1]);  
  async_work_group_copy(roundKeyB, roundKey, (size_t)11, copies[2]);
  wait_group_events(3, copies);
	private uchar16 blockA, blockB;
  const uint preIndex = ioOffset + threadIdx;
	for(unsigned int blk = 0; blk < blocksPerThread; blk++)
	{
		const unsigned int globalIndex = preIndex + blk * localSize;
		blockB = input[globalIndex] ^ roundKeyB[0];
		// combined SBox/ShiftRows/MixColumns
		for(unsigned int r = 1; r < 10; ++r)
		{
			blockA  = (uchar16)(
				mixColB[blockB.s0]       ^ mixColB[blockB.s5 + 256] ^ 
				mixColB[blockB.sa + 512] ^ mixColB[blockB.sf + 768],
				mixColB[blockB.s4]       ^ mixColB[blockB.s9 + 256] ^
				mixColB[blockB.se + 512] ^ mixColB[blockB.s3 + 768],
				mixColB[blockB.s8]       ^ mixColB[blockB.sd + 256] ^
				mixColB[blockB.s2 + 512] ^ mixColB[blockB.s7 + 768],
				mixColB[blockB.sc]       ^ mixColB[blockB.s1 + 256] ^
				mixColB[blockB.s6 + 512] ^ mixColB[blockB.sb + 768]);
			blockB = blockA ^ roundKeyB[r];
		}
		// S-Boxes + transpose + last shiftRows + last round key
		output[globalIndex] = (uchar16)(
				SBoxB[blockB.s0],SBoxB[blockB.s5],SBoxB[blockB.sa],SBoxB[blockB.sf],
				SBoxB[blockB.s4],SBoxB[blockB.s9],SBoxB[blockB.se],SBoxB[blockB.s3],
				SBoxB[blockB.s8],SBoxB[blockB.sd],SBoxB[blockB.s2],SBoxB[blockB.s7],
				SBoxB[blockB.sc],SBoxB[blockB.s1],SBoxB[blockB.s6],SBoxB[blockB.sb])
				^ roundKeyB[10];
	}
}

