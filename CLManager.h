/** Copyright (c) 2014 drs.mandrake@gmail.com
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
* This file is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*/

#pragma once

#include <CL/cl.h>
#include <string>
#include <list>

class CLManager
{
  public:

  typedef enum {amd, nv, intel, unknown} clVendor;
  enum _bufType{rw, r, w} bufType;

  class deviceDescriptor
  {
  public:
    cl_device_id m_deviceId;
    char* m_deviceName;
    size_t m_workGroupSize;
    uint m_numDimensions;
    size_t* m_workItemSize;
    cl_uint m_maxComputeUnits;
    cl_ulong m_globMem;
    cl_ulong m_locMem;

    class constructionError{};
    deviceDescriptor(cl_device_id d, uint maxNameSize);
    ~deviceDescriptor();
    void print();
  };



  class KernelArgument
  {
  public:
    size_t  m_size;
    void*   m_ptr;
    KernelArgument(size_t p_size, void* p_ptr):m_size(p_size), m_ptr(p_ptr){}
  };


    cl_context        m_context;        // CL context 

    deviceDescriptor* m_deviceDesc;

    cl_command_queue  m_commandQueue;   // CL command queue
    cl_program        m_program;        // CL program


    cl_ulong      m_usedLocalMemory; 
    cl_ulong      m_availableLocalMemory; 
    cl_ulong      m_neededLocalMemory;

    bool m_initialized;
    bool m_programBuilt;

    CLManager();
    ~CLManager();
    void buildProgram(const std::string & p_programName);
    cl_kernel createKernel(std::string p_kernelEntryPoint);
    static cl_uchar& getucharRef(cl_uchar4& p_vect, unsigned int p_coordinate);
    static void checkOutput(int res, const char* p_errorMessage);
    void enqueueCall(cl_kernel p_kernel, std::list<KernelArgument>& p_argumentList,
      size_t p_numDimensions, size_t* p_itemSizes, size_t* p_groupSizes, int numPreviousEvents,
      cl_event* p_previousEvents, cl_event* p_nextEvent);
    void enqueueNakedCall(cl_kernel p_kernel, size_t p_numDimensions, size_t* p_itemSizes,
      size_t* p_groupSizes, int numPreviousEvents, cl_event* p_previousEvents, cl_event* p_nextEvent);
    template<class T>
    void createHostBufferFromPointer(enum _bufType p_devBufType, unsigned int p_numElements,
      cl_mem& po_buf, T* p_bufAddr);
    template<class T>
    void createHostConstantBufferFromPointer(unsigned int p_numElements, cl_mem& po_buf,
      T* p_bufAddr);
    template<class T>
    void createHostPinnedBuffer(enum _bufType p_devBufType, size_t p_bufferSize,
      cl_mem& po_buf, T*& po_memArea);
    template<class T>
    void createDeviceBuffer(enum _bufType p_devBufType, size_t p_bufferSize, cl_mem& po_buf,
      T* p_buf);
    template<class T>
    void createDeviceBufferFromHostData(enum _bufType p_devBufType, size_t p_bufferSize,
      cl_mem& po_buf, T* p_bufAddr);
    bool checkNeededLocalMemory(cl_kernel p_kernel, size_t p_neededLocalMemory);
    template<class T>
    void enqueueWrite(cl_mem& p_buf, cl_bool p_blockingWrite, size_t p_offset,
      size_t p_copySize, const T* p_ptr, cl_uint p_numPreviousEvent, cl_event* p_ptrPreviousEvent,
      cl_event* p_ptrNextEvent);
    template<class T>
    void enqueueRead(cl_mem& p_buf, cl_bool p_blockingRead, size_t p_offset,
      size_t p_copySize, const T* p_ptr, cl_uint p_numPreviousEvent, cl_event* p_ptrPreviousEvent,
      cl_event* p_ptrNextEvent);
    void enqueueBarrier();
    void finish();
    cl_platform_id getPlatformId();
    deviceDescriptor *newDeviceDesc(cl_platform_id p_platformId, bool p_gpu, int&status);
    cl_ulong getTotalGlobalMemory () const;
};

template<class T>
void CLManager::createHostBufferFromPointer(enum _bufType p_devBufType, unsigned int p_numElements,
      cl_mem& po_buf, T* p_bufAddr)
{
  cl_int status = 0;
  cl_mem_flags l_flags = 0;
  switch(p_devBufType)
  {
  case rw:
    l_flags = CL_MEM_READ_WRITE;
    break;
  case r:
    l_flags = CL_MEM_READ_ONLY;
    break;
  case w:
    l_flags = CL_MEM_WRITE_ONLY;
  }

  po_buf = clCreateBuffer(m_context, l_flags  | CL_MEM_USE_HOST_PTR, p_numElements,
    (void*) p_bufAddr, &status);
  checkOutput(status, "clCreateBuffer failed.");
}

template<class T>
void CLManager::createHostConstantBufferFromPointer(unsigned int p_numElements, cl_mem& po_buf,
    T* p_bufAddr)
{
  createHostBufferFromPointer(r, p_numElements, po_buf, p_bufAddr);
}

template<class T>
void CLManager::createHostPinnedBuffer(enum _bufType p_devBufType, size_t p_bufferSize,
    cl_mem& po_buf, T*& po_memArea)
{
  cl_int status = 0;
  cl_mem_flags l_flags = 0;
  cl_map_flags l_mapFlags = 0;
  switch(p_devBufType)
  {
  case rw:
    l_flags = CL_MEM_READ_WRITE;
    l_mapFlags = CL_MAP_READ | CL_MAP_WRITE;
    break;
  case r:
    l_flags = CL_MEM_READ_ONLY;
    l_mapFlags = CL_MAP_WRITE;
    break;
  case w:
    l_flags = CL_MEM_WRITE_ONLY;
    l_mapFlags = CL_MAP_READ;
  }

  po_buf = clCreateBuffer(m_context, l_flags  | CL_MEM_ALLOC_HOST_PTR, p_bufferSize,
    NULL, &status);
  checkOutput(status, "clCreateBuffer failed.");
  po_memArea  = static_cast<T*>(clEnqueueMapBuffer(m_commandQueue, po_buf, CL_TRUE, l_mapFlags, 0, 
    p_bufferSize, 0, NULL, NULL, &status));
  checkOutput(status, "clEnqueueMapBuffer failed.");
}


template<class T>
void CLManager::createDeviceBufferFromHostData(enum _bufType p_devBufType, size_t p_bufferSize,
    cl_mem& po_buf, T* p_bufAddr)
{
  cl_int status = 0;
  cl_mem_flags l_flags = 0;
  switch(p_devBufType)
  {
  case rw:
    l_flags = CL_MEM_READ_WRITE;
    break;
  case r:
    l_flags = CL_MEM_READ_ONLY;
    break;
  case w:
    l_flags = CL_MEM_WRITE_ONLY;
  }
  po_buf = clCreateBuffer(m_context, l_flags | CL_MEM_COPY_HOST_PTR, 
    p_bufferSize * sizeof(T), (void*) p_bufAddr, &status);
  checkOutput(status, "clCreateBuffer failed.");
}

template<class T>
void CLManager::enqueueWrite(cl_mem& p_buf, cl_bool p_blockingWrite, size_t p_offset,
  size_t p_copySize, const T* p_ptr, cl_uint p_numPreviousEvent, cl_event* p_ptrPreviousEvent,
  cl_event* p_ptrNextEvent)
{
  cl_int status = clEnqueueWriteBuffer(m_commandQueue, p_buf, p_blockingWrite, sizeof(T) * p_offset,
    sizeof(T) * p_copySize, const_cast<unsigned char*>(p_ptr), p_numPreviousEvent, p_ptrPreviousEvent,
    p_ptrNextEvent);
  CLManager::checkOutput(status, "clEnqueueWriteBuffer failed.");
}

template<class T>
void CLManager::enqueueRead(cl_mem& p_buf, cl_bool p_blockingRead, size_t p_offset,
  size_t p_copySize, const T* p_ptr, cl_uint p_numPreviousEvent, cl_event* p_ptrPreviousEvent,
  cl_event* p_ptrNextEvent)
{
  cl_int status = clEnqueueReadBuffer(m_commandQueue, p_buf, p_blockingRead, sizeof(T) * p_offset,
    sizeof(T) * p_copySize, const_cast<unsigned char*>(p_ptr), p_numPreviousEvent, p_ptrPreviousEvent,
    p_ptrNextEvent);
  CLManager::checkOutput(status, "clEnqueueReadBuffer failed.");
}

template <class T>
void CLManager::createDeviceBuffer(enum _bufType p_devBufType, size_t p_bufferSize,
    cl_mem& po_buf, T* )
{
  cl_int status = 0;
  cl_mem_flags l_flags = 0;
  switch(p_devBufType)
  {
  case rw:
    l_flags = CL_MEM_READ_WRITE;
    break;
  case r:
    l_flags = CL_MEM_READ_ONLY;
    break;
  case w:
    l_flags = CL_MEM_WRITE_ONLY;
  }
  po_buf = clCreateBuffer(m_context, l_flags, sizeof(T) * p_bufferSize, 0, &status);
  checkOutput(status, "clCreateBuffer failed.");
}
