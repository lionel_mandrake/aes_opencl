/**
 * A simple C++ wrapper around the C OpenCL API.
 *
 * Copyright (c) 2014 drs.mandrake@gmail.com
 *
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

// #define OPENCL_1_1_COMPAT

#include "CLManager.h"

#include<fstream>
#include<iostream>
#include<sstream>
#include<cstdlib>
#include<string.h>
#include<memory>

#include "genericException.h"

using namespace std;

#ifdef OPENCL_AMD
  const CLManager::clVendor preferredVendor = CLManager::amd;
#else
#ifdef OPENCL_NV
  const CLManager::clVendor preferredVendor = CLManager::nv;
#else
#ifdef OPENCL_INTEL
  const CLManager::clVendor preferredVendor = CLManager::intel;
#else
  const CLManager::clVendor preferredVendor = CLManager::unknown;
#endif
#endif
#endif

CLManager::CLManager() :
  m_context(nullptr),
  m_deviceDesc(nullptr),
  m_commandQueue(nullptr),
  m_initialized(false),
  m_programBuilt(false)
{
  cl_int status = 0;

  try
  {
    cl_platform_id platform = getPlatformId();
    cl_context_properties cprops[3] =
    {
      CL_CONTEXT_PLATFORM,
      (cl_context_properties) platform,
      0
    };
    m_deviceDesc = newDeviceDesc(platform, true, status);
    // if there is no GPU device on the selected platform, try to fall back to CPU
    // note: unsupported with NVIDIA
    if (status != CL_SUCCESS)
    {
      cout << "Unsupported GPU device (status =" << status << "); falling back to CPU" << endl;
      delete m_deviceDesc;
      m_deviceDesc = newDeviceDesc(platform, false, status);
      CLManager::checkOutput(status, "Fallback failed.");
    }
    m_context = clCreateContext(cprops, 1, &m_deviceDesc->m_deviceId, 0, 0, &status);
    checkOutput(status, "clCreateContext / clCreateContextFromType failed.");
    cl_command_queue_properties prop = 0;
    prop |= CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE;
    m_commandQueue = clCreateCommandQueue(m_context, m_deviceDesc->m_deviceId, prop, &status);
    CLManager::checkOutput(status, "clCreateCommandQueue failed.");
    m_initialized = true;
  }
  catch (exception & e)
  {
    cout << e.what() << endl;
  }
}

cl_platform_id CLManager::getPlatformId()
{
  string platformVendor;
  // set a preferred platform vendor. If no platform matches the preferred 
  // platform name, platform 0 will be selected
  switch(preferredVendor)
  {
  case amd:
    platformVendor = "Advanced Micro Devices, Inc.";
    break;
  case nv:
    platformVendor = "NVIDIA Corporation";
    break;
  case intel:
    platformVendor = "Intel(R) Corporation";
    break;
  default:
    platformVendor = "unknown";
    break;
  }
  cl_platform_id* platforms = nullptr;
  //TempArrayCleaner<cl_platform_id> platformsCleaner(platforms);
  unique_ptr<cl_platform_id[]> platformsCleaner(platforms);
  cl_uint numPlatforms;
  cl_platform_id platform = 0;
  int status = clGetPlatformIDs(0, 0, &numPlatforms);
  CLManager::checkOutput(status, "clGetPlatformIDs failed.");
  unsigned int i_selected = 0;
  cout << "Number of available platforms for OpenCL: " << numPlatforms << endl;
  if (0 < numPlatforms)
  {
    platforms = new cl_platform_id[numPlatforms];
    platformsCleaner.reset(platforms);
    status = clGetPlatformIDs(numPlatforms, platforms, 0);
    CLManager::checkOutput(status, "clGetPlatformIDs failed.");
    char vendor[100];
    char version[100];
    for (unsigned int i = 0; i < numPlatforms; ++i)
    {
      status = clGetPlatformInfo(platforms[i],
          CL_PLATFORM_VENDOR,
          sizeof (vendor),
          vendor,
          NULL);
      CLManager::checkOutput(status, "clGetPlatformInfo failed.");
      status = clGetPlatformInfo(platforms[i],
          CL_PLATFORM_VERSION,
          sizeof (version),
          version,
          NULL);
      CLManager::checkOutput(status, "clGetPlatformInfo failed.");
      cout << "platform " << i + 1 << " / " << numPlatforms << ":" << endl;
      cout << " vendor: " << vendor << endl;
      cout << " version: " << version << endl;
      cout.flush();
      if (string(vendor) == platformVendor)
      {
        i_selected = i;
      }
    }
    cout << "Selected platform " << i_selected + 1 << endl << endl;
    platform = platforms[i_selected];
  }
  return platform;
}

CLManager::deviceDescriptor* CLManager::newDeviceDesc(
    cl_platform_id p_platformId, bool p_gpu, int&status)
{
  const size_t maxNumDevices = 4;
  //cl_device_id* devices = new cl_device_id[maxNumDevices];
  //TempArrayCleaner<cl_device_id> devicesCleaner(devices);
  unique_ptr<cl_device_id[]> devicesCleaner(new cl_device_id[maxNumDevices]);
  cl_device_id* devices = devicesCleaner.get();
  cl_uint numDevices;
  status = CL_SUCCESS;
  cl_bitfield device_type = p_gpu? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU;
  status = clGetDeviceIDs(p_platformId, device_type, maxNumDevices, devices, &numDevices);
  if(status != CL_SUCCESS) return nullptr;
  unsigned int i_selected = 0;
  cout << "Number of available devices on the selected platform: " << numDevices << endl;

  deviceDescriptor** desc_array = nullptr;

  desc_array = new deviceDescriptor*[numDevices];
  for (unsigned int i = 0; i < numDevices; ++i)
  {
    cout << "device " << i + 1 << " / " << numDevices << ":" << endl;
    desc_array[i] = new deviceDescriptor(devices[i], 100);
    desc_array[i]->print();
  }

  //i_selected = ...

  for (unsigned int i = 0; i < numDevices; ++i)
  {
    if(i != i_selected) delete desc_array[i];
  }
  deviceDescriptor* d = desc_array[i_selected];
  delete [] desc_array;
  cout << "Selected device " << i_selected + 1 << endl << endl;
  return d;
}

void CLManager::buildProgram(const string & p_programName)
{
  int status;
  cout << "opening kernel file " << p_programName << endl;
  ifstream kernelFile(p_programName.c_str());
  if (!kernelFile.is_open())
  {
    throw GenericException("Kernel file could not be opened");
  }
  string line;
  string prg;
  while (!kernelFile.eof())
  {
    getline(kernelFile, line);
    prg += line + "\n";
  }
  const char* source = prg.c_str();

  size_t sourceSize = prg.size();
  m_program = clCreateProgramWithSource(m_context, 1, &source, &sourceSize, &status);

  CLManager::checkOutput(status, "clCreateProgramWithSource failed.");
  const char* opts = preferredVendor == nv ? "-cl-mad-enable -cl-strict-aliasing" : "";
  status = clBuildProgram(m_program, 1, &m_deviceDesc->m_deviceId, opts, 0, 0);
  const size_t l_maxLogSize = 1 << 20;
  //char* programLog = new char[l_maxLogSize];
  //TempArrayCleaner<char> programLogCleaner(programLog);

  unique_ptr<char[]> programLogCleaner(new char[l_maxLogSize]);
  char* programLog = programLogCleaner.get();


  size_t logStatus = 0;
  clGetProgramBuildInfo(m_program, m_deviceDesc->m_deviceId, CL_PROGRAM_BUILD_LOG, l_maxLogSize, programLog, &logStatus);
  if (logStatus > 0)
  {
    cout << "Program build log:";
    //cout << endl;
    cout << programLog << endl << endl;
  }
  CLManager::checkOutput(status, "clBuildProgram failed.");

  m_programBuilt = true;
}

cl_kernel CLManager::createKernel(string p_kernelEntryPoint)
{
  cl_int status;
  cl_kernel l_kernel = clCreateKernel(m_program, p_kernelEntryPoint.c_str(), &status);
  CLManager::checkOutput(status, "clCreateKernel failed.");
  return l_kernel;
}

CLManager::~CLManager()
{
  delete m_deviceDesc;
  if (m_programBuilt)
  {
    CLManager::checkOutput(clReleaseProgram(m_program), "clReleaseProgram failed.");
  }
  CLManager::checkOutput(clReleaseCommandQueue(m_commandQueue), "clReleaseCommandQueue failed.");
  CLManager::checkOutput(clReleaseContext(m_context), "clReleaseContext failed.");
}

cl_uchar& CLManager::getucharRef(cl_uchar4& p_vect, unsigned int p_coordinate)
{
  return p_vect.s[p_coordinate];
}

void CLManager::checkOutput(int res, const char* p_errorMessage)
{
  if (res != CL_SUCCESS)
  {
    stringstream s;
    s << p_errorMessage << endl << "Error code: " << res << endl;
    GenericException e(s.str().c_str());
    throw e;
  }
}

void CLManager::enqueueCall(cl_kernel p_kernel, list<KernelArgument>& p_argumentList,
    size_t p_numDimensions, size_t* p_itemSizes, size_t* p_groupSizes, int numPreviousEvents,
    cl_event* p_previousEvents, cl_event* p_nextEvent)
{
  if (p_numDimensions > m_deviceDesc->m_numDimensions)
  {
    throw GenericException("Too many dimensions for CL Kernel");
  }
  size_t l_itemSize = 1;
  if (p_itemSizes)
  {
    for (unsigned int i = 0; i < p_numDimensions; i++)
    {
      if (p_itemSizes[i] > m_deviceDesc->m_workItemSize[i])
      {
        throw GenericException("At least one work item dimension is too large");
      }
      l_itemSize *= p_itemSizes[i];
    }
    if (l_itemSize > m_deviceDesc->m_workGroupSize)
    {
      throw GenericException("Unsupported: The requested number of work items is too large");
    }
  }

  unsigned int currentArg = 0;
  for (list<KernelArgument>::iterator it = p_argumentList.begin(); it != p_argumentList.end(); it++)
  {
    unsigned int status = clSetKernelArg(p_kernel, currentArg++, it->m_size, it->m_ptr);
    checkOutput(status, "clSetKernelArg failed");
  }

  unsigned int status = clEnqueueNDRangeKernel(m_commandQueue, p_kernel, p_numDimensions,
      NULL, p_groupSizes, p_itemSizes, numPreviousEvents, p_previousEvents, p_nextEvent);
  checkOutput(status, "clEnqueueNDRangeKernel failed");
}

void CLManager::enqueueNakedCall(cl_kernel p_kernel,
    size_t p_numDimensions, size_t* p_itemSizes, size_t* p_groupSizes, int numPreviousEvents,
    cl_event* p_previousEvents, cl_event* p_nextEvent)
{
  unsigned int status = clEnqueueNDRangeKernel(m_commandQueue, p_kernel, p_numDimensions,
      NULL, p_groupSizes, p_itemSizes, numPreviousEvents, p_previousEvents, p_nextEvent);
  checkOutput(status, "clEnqueueNDRangeKernel failed");
}

void CLManager::enqueueBarrier()
{
  // For OpenCL 1.1
#ifdef OPENCL_1_1_COMPAT
  clEnqueueBarrier(m_commandQueue);
#else
  // For OpenCL 1.2
  clEnqueueBarrierWithWaitList(m_commandQueue, 0, nullptr, nullptr);
#endif
}

void CLManager::finish()
{
  clFinish(m_commandQueue);
}

bool CLManager::checkNeededLocalMemory(cl_kernel p_kernel, size_t p_neededLocalMemory)
{
  cl_int status = clGetKernelWorkGroupInfo(p_kernel, m_deviceDesc->m_deviceId, CL_KERNEL_LOCAL_MEM_SIZE,
      sizeof (cl_ulong), &m_usedLocalMemory, NULL);
  checkOutput(status, "clGetKernelWorkGroupInfo failed.(usedLocalMemory)");

  m_availableLocalMemory = m_deviceDesc->m_locMem - m_usedLocalMemory;
  return p_neededLocalMemory <= m_availableLocalMemory;
}

cl_ulong CLManager::getTotalGlobalMemory() const
{
  cl_ulong size;
  clGetDeviceInfo(m_deviceDesc->m_deviceId, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof (cl_ulong),
      &size, NULL);
  return size;
}

CLManager::deviceDescriptor::deviceDescriptor(cl_device_id d, uint maxNameSize)
  {
    m_deviceId = d;
    m_deviceName = new char[maxNameSize + 1];
    int status = clGetDeviceInfo(d, CL_DEVICE_NAME, maxNameSize, m_deviceName, 0);
    if(status != CL_SUCCESS) throw constructionError();
    status = clGetDeviceInfo(d, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof (m_workGroupSize),
        &m_workGroupSize, 0);
    if(status != CL_SUCCESS) throw constructionError();
    // get max work group / item dimension
    clGetDeviceInfo(d, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof (cl_uint),
        (void *) &m_numDimensions, NULL);
    if(status != CL_SUCCESS) throw constructionError();
    m_workItemSize = new size_t[m_numDimensions];
    status = clGetDeviceInfo(d, CL_DEVICE_MAX_WORK_ITEM_SIZES, m_numDimensions * sizeof(size_t),
        m_workItemSize, 0);
    if(status != CL_SUCCESS) throw constructionError();
    status = clGetDeviceInfo(d, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof (m_maxComputeUnits),
        &m_maxComputeUnits, 0);
    if(status != CL_SUCCESS) throw constructionError();
    clGetDeviceInfo(d, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof (cl_ulong),
        (void *) &m_globMem, NULL);
    if(status != CL_SUCCESS) throw constructionError();
    clGetDeviceInfo(d, CL_DEVICE_LOCAL_MEM_SIZE, sizeof (cl_ulong),
        (void *) &m_locMem, NULL);
    if(status != CL_SUCCESS) throw constructionError();
  }

  CLManager::deviceDescriptor::~deviceDescriptor()
  {
    delete [] m_workItemSize;
    delete [] m_deviceName;
  }

  void CLManager::deviceDescriptor::print()
  {
    cout << " name: " << m_deviceName << endl;
    cout << " Maximum work group size: " << m_workGroupSize << endl;
    cout << " Maximum number of dimensions: " << m_numDimensions << endl;
    cout << " Maximum work item sizes: ";
    for(size_t j=0; j< m_numDimensions;j++)
    {
      cout << m_workItemSize[j];
      if (j + 1 < m_numDimensions) cout << ", ";
    }
    cout << endl;
    cout << " Maximum number of parallel compute units: " << m_maxComputeUnits << endl;
    cout << " Maximum global memory size: " << m_globMem << " bytes" << endl;
    cout << " Maximum local memory size: " << m_locMem << " bytes" << endl;
    cout.flush();
  }

