/**
 * A implementation of AES in OpenCL. Encrypts blocks of pseudorandom data and compares the
 * result and the speed obtained to the results of OpenSSL (which is therefore required).
 * Linux-only code. Tested on Ubuntu 14.04 x64 with both Intel and AMD GPU hardware. Should work
 * with Nvidia hardware.
 *
 * This code, despite being fast enough to do full-duplex 10GB/s with a threadable mode such as
 * CTR on recent mid-range GPUs, is obsoleted by modern CPU acceleration for AES,
 * e.g. AES-NI on x86/x86_64 CPUs.
 * This is nevertheless an interesting use-case for GPUs, and could be adapted to
 * other block ciphers or hash functions.
 *
 * Copyright (c) 2014 drs.mandrake@gmail.com.
 *
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#include "main.hpp"

#include "genericException.h"
#include "Timer.h"

#include <iostream>
#include <list>
#include <exception>
#include <cmath>
#include <assert.h>
#include <string.h>

#include <openssl/evp.h>

using namespace AES;
using namespace std;

const bool g_usePinnedMemory = true;
const bool g_useDummyKernel = false;

int main(int argc, char * argv[])
{
  AESEncrypt clAESEncryptDecrypt;
  try
  {
    clAESEncryptDecrypt.setup();
    clAESEncryptDecrypt.runCLKernel();
    clAESEncryptDecrypt.analyzeResults();
    clAESEncryptDecrypt.cleanupCL();
  }
  catch(GenericException & e)
  {
    cout << e.what() << endl;
  }
}

void AESEncrypt::setup()
{
  if(!m_CLManager.m_initialized)
  {
    cout << "Error when initializing OpenCL. Exiting." << endl;
    exit(1);
  }
  // name of the kernel source file
  string l_programName;
  l_programName = g_useDummyKernel ? "dummy.cl" : "AES.cl";
  m_CLManager.buildProgram(l_programName);
  m_kernel = m_CLManager.createKernel("AESEncrypt");
  setupTablesAndKey();
  createHostBuffers();
  fillInput();
}

void AESEncrypt::setupTablesAndKey()
{
  // init mixCol tables
  for(unsigned int i = 0; i < 256; i++)
  {
    cl_uchar v = static_cast<cl_uchar>(i);
    CLManager::getucharRef(mixCol[v], 0) = galoisMultiplication(v, 2);
    CLManager::getucharRef(mixCol[v], 1) = v;
    CLManager::getucharRef(mixCol[v], 2) = v;
    CLManager::getucharRef(mixCol[v], 3) = galoisMultiplication(v, 3);

    for(unsigned int j = 0 ; j < 4; j++)
    {
      CLManager::getucharRef(mixCol[v + 256], j) = CLManager::getucharRef(mixCol[v], (j+3)&3);
      CLManager::getucharRef(mixCol[v + 512], j) = CLManager::getucharRef(mixCol[v], (j+2)&3);
      CLManager::getucharRef(mixCol[v + 768], j) = CLManager::getucharRef(mixCol[v], (j+1)&3);
    }
  }
  for(unsigned int v = 0; v < 256; v++)
  {
    for(unsigned int table = 0 ; table < 4; table++)
    {
      for(unsigned int j = 0 ; j < 4; j++)
      {
        CLManager::getucharRef(subBytesAndMixCol[v + 256 * table], j) =
          CLManager::getucharRef(mixCol[sbox[v] + 256 * table], j);
      }
    }
  }

  for(cl_uchar j = 0 ; j < 16; j++) m_key[j]  = j;

  // expand the key
  keyExpansion(m_key, m_expandedKey, m_keySize, m_expandedKeySize);
  for(cl_uint i = 0; i < m_rounds + 1; ++i)
  {
    createRoundKey(m_expandedKey + m_keySize * i, m_roundKey + m_keySize * i);
  }
}

void AESEncrypt::fillInput()
{
  for(size_t i = 0 ; i < 16; i++)
  {
    m_input[i] =  (cl_uchar)(i | i << 4);
  }
  for(size_t i = 16; i < m_inputSize; i++)
  {
    m_input[i] = rand() & 0xFF;
  }
}

void AESEncrypt::createHostBuffers()
{
  const unsigned int l_totalPoolSize = m_taskPoolSize * m_taskProcessingByteSize;

  if(g_usePinnedMemory)
  {
    m_CLManager.createHostPinnedBuffer(CLManager::r, l_totalPoolSize, m_inputBuffer, m_input);
    m_CLManager.createHostPinnedBuffer(CLManager::w, l_totalPoolSize, m_outputBuffer, m_output);
  }
  else
  {
    m_input = new cl_uchar[l_totalPoolSize];
    m_output = new cl_uchar[l_totalPoolSize];
  }
  m_CLManager.createDeviceBuffer(CLManager::rw,  l_totalPoolSize, cmDevBufInOut, m_input);
  m_CLManager.createDeviceBufferFromHostData(CLManager::r, 11 * 16, cmDevRoundKeys, m_expandedKey);
  m_CLManager.createDeviceBufferFromHostData(CLManager::r, 256, cmDevSBox, sbox);
  m_CLManager.createDeviceBufferFromHostData(CLManager::r, 1024, cmDevMixCol, subBytesAndMixCol);
}

void AESEncrypt::runCLKernel()
{
  unsigned int neededLocalMemory = m_mixColumnBufferSize + m_sboxBufferSize + m_expandedKeySize;
  if(!m_CLManager.checkNeededLocalMemory(m_kernel, neededLocalMemory))
  {
    throw GenericException("Insufficient local memory on device.");
  }
  cout << "Beginning of encryption on the GPU" << endl;

  Timer t(true);
  enqueueWriteToGPU(0, 0, 0, 0);
  enqueueWriteToGPU(1 % m_taskPoolSize, 0, 0, 1);
  enqueueEncryptionKernelCall(0, 1, 0, 2);
  for(unsigned int taskIndex = 0; taskIndex < m_numTasks; taskIndex++)
  {
    unsigned int eventOffset = 3 * (taskIndex % 2); // 0 or 3
    unsigned int nextEventOffset = 3 - eventOffset; // 0 or 3, the opposite of eventOffset
    // index of the block sent to the device
    unsigned int writeTaskIndex = (taskIndex + 2) % m_taskPoolSize;
    // index of the block processed by the device
    unsigned int computeTaskIndex = (taskIndex + 1) % m_taskPoolSize;
    // index of the block read from the device
    unsigned int readTaskIndex = taskIndex % m_taskPoolSize;
    enqueueReadFromGPU(readTaskIndex, 3, eventOffset, nextEventOffset);
    if(taskIndex < m_numTasks - 1)
    {
      enqueueEncryptionKernelCall(computeTaskIndex, 3, eventOffset, nextEventOffset + 2);
    }
    if(taskIndex < m_numTasks - 2)
    {
      enqueueWriteToGPU(writeTaskIndex, 3, eventOffset, nextEventOffset + 1);
    }
  }
  CLManager::checkOutput(clWaitForEvents(6, m_events), "clWaitForEvents failed.");
  m_GPUTime = t.stop();
  cout << "End of encryption on the GPU" << endl;

  for(size_t i = 0; i < m_eventSize; i++) 
  {
    clReleaseEvent(m_events[i]);
  }
}

void AESEncrypt::enqueueWriteToGPU(
    unsigned int taskIndexInPool,
    int numPreviousEvents,
    int previousEventIndex,
    int nextEventIndex)
{
  cl_event* l_previousEvents = numPreviousEvents == 0 ? nullptr : &m_events[previousEventIndex];
  m_CLManager.enqueueWrite(cmDevBufInOut, CL_FALSE, 
    m_taskProcessingByteSize * taskIndexInPool, m_taskProcessingByteSize,
    m_input +  m_taskProcessingByteSize * taskIndexInPool, numPreviousEvents,
    l_previousEvents, &m_events[nextEventIndex]);
}

void AESEncrypt::enqueueReadFromGPU(
    unsigned int taskIndexInPool,
    int numPreviousEvents,
    int previousEventIndex,
    int nextEventIndex)
{
  cl_event* l_previousEvents = numPreviousEvents == 0 ? nullptr : &m_events[previousEventIndex];
  m_CLManager.enqueueRead(cmDevBufInOut, CL_FALSE, 
    m_taskProcessingByteSize * taskIndexInPool, m_taskProcessingByteSize,
    m_output +  m_taskProcessingByteSize * taskIndexInPool, numPreviousEvents,
    l_previousEvents, &m_events[nextEventIndex]);
}

void AESEncrypt::enqueueEncryptionKernelCall(
    unsigned int taskIndexInPool,
    int numPreviousEvents,
    int previousEventIndex,
    int nextEventIndex)
{
  // Create argument list
  list<CLManager::KernelArgument> l_argumentList;
  unsigned int l_taskBlockOffset = taskIndexInPool * m_taskProcessingByteSize / 16;
  l_argumentList.push_back(CLManager::KernelArgument(sizeof(cl_mem), (void *)&cmDevBufInOut));
  l_argumentList.push_back(CLManager::KernelArgument(sizeof(cl_mem), (void *)&cmDevBufInOut));
  if(!g_useDummyKernel)
  {
    l_argumentList.push_back(CLManager::KernelArgument(sizeof(cl_mem), (void *)&cmDevRoundKeys));
    l_argumentList.push_back(CLManager::KernelArgument(sizeof(cl_mem), (void *)&cmDevSBox));
    l_argumentList.push_back(CLManager::KernelArgument(sizeof(cl_mem), (void *)&cmDevMixCol));
    l_argumentList.push_back(CLManager::KernelArgument(m_sboxBufferSize,      0));
    l_argumentList.push_back(CLManager::KernelArgument(m_mixColumnBufferSize, 0));
    l_argumentList.push_back(CLManager::KernelArgument(m_expandedKeySize,     0));
    l_argumentList.push_back(CLManager::KernelArgument(sizeof(cl_uint), (void *)&m_rounds));
  }
  l_argumentList.push_back(CLManager::KernelArgument(sizeof(cl_uint), (void *)&l_taskBlockOffset));
  l_argumentList.push_back(CLManager::KernelArgument(sizeof(cl_uint), (void *)&m_blocksPerThread));
  cl_event* l_previousEvents = numPreviousEvents == 0 ? nullptr : &m_events[previousEventIndex];
  m_CLManager.enqueueCall(m_kernel, l_argumentList, 1, &m_localThreads, &m_globalThreads, 
    numPreviousEvents, l_previousEvents, &m_events[nextEventIndex]);
}

void AESEncrypt::analyzeResults()
{
  m_GPUTime /= double(m_numRuns);
  cout << "GPU CL Kernel time per encryption run: " << m_GPUTime << endl << endl;
  cout << "GPU Throughput: " << double(m_inputSize) / (double(1 << 27) * m_GPUTime) <<
    " Gb/s" << endl << endl;

  // first ciphertext block should be refCiphertext
  bool l_firstctBlockOk = memcmp(m_output, refCiphertext, 16) == 0;
  cl_uchar* l_verificationInput = nullptr;
  if(g_usePinnedMemory)
  {
    // copy the input data
    // otherwise the use of pinned memory makes the encryption very slow.
    l_verificationInput = new cl_uchar[m_inputSize];
    memcpy(l_verificationInput, m_input, m_inputSize);
  }
  else
  {
    l_verificationInput = m_input;
  }
  m_verificationOutput = new cl_uchar[m_inputSize + 16];
  for(size_t i = 0; i < m_inputSize; i++)
  {
    m_verificationOutput[i] = 0;
  }
  cout << "Beginning of encryption on the CPU" << endl;
  Timer t(true);
  size_t l_numCPURuns = m_numRuns;
  cout << "log_2 of input size(bytes): " << log(m_inputSize)/log(2.) << endl;
  for(size_t i = 0; i < l_numCPURuns; i++)
  {
    EVP_CIPHER_CTX ctx;
    EVP_EncryptInit(&ctx, EVP_aes_128_ecb(), m_key, 0);
    int l_offset = 0;
    {
      int l_outSize;
      EVP_EncryptUpdate(&ctx, m_verificationOutput, &l_outSize, l_verificationInput, m_inputSize);
      l_offset += l_outSize;
    }
    {
      int l_outSize;
      EVP_EncryptFinal(&ctx, m_verificationOutput + l_offset, &l_outSize);
      l_offset += l_outSize;
    }
  }
  cout << "End of encryption on the CPU" << endl;

  if(g_usePinnedMemory) delete [] l_verificationInput;

  m_CPUTime = t.stop() / l_numCPURuns;
  cout << "Average CPU time per encryption run (1 thread, OpenSSL): " << m_CPUTime << endl;
  cout << "CPU throughput: " << double(m_inputSize) /
      (double(1 << 27) * m_CPUTime) << " Gb/s" << endl << endl;
  cout << "GPU vs. CPU Speedup: " << m_CPUTime / m_GPUTime << endl << endl;

  if(l_firstctBlockOk)
  {
    cout << "Ok: First GPU ciphertext block is correct";
  }
  else
  {  
    cout << "KO: First GPU ciphertext block value is WRONG";
  }
  cout  << endl;

  if(memcmp(m_output, m_verificationOutput, m_inputSize) == 0)
  {
    cout << "Ok: CPU and GPU outputs are identical";
  }
  else
  {  
    cout << "KO: CPU and GPU outputs are DIFFERENT";
  }
  cout  << endl << endl;
}

AESEncrypt::AESEncrypt():
  m_input(nullptr),
  m_output(nullptr),
  m_key(nullptr),
  m_expandedKey(nullptr),
  m_roundKey(nullptr),
  m_verificationOutput(nullptr),
  m_CLManager(),
  m_events(nullptr)
{
  cout.precision(4);

  m_rounds = 10;
  m_keySize = 16;
  m_key = new cl_uchar[m_keySize];
  m_expandedKeySize = (m_rounds + 1) * m_keySize;  
  m_expandedKey = new cl_uchar[m_expandedKeySize];
  m_roundKey    = new cl_uchar[m_expandedKeySize];

  m_GPUTime = 0;

  // pool definition
  // log of the data byte size processed by one cl kernel
  // cannot be too large (constrained by device)
  uint l_taskProcessingLogByteSize = 24;
  // log of the number of calls to the kernel
  uint l_taskPoolLogSize = 3;
  m_taskProcessingByteSize =  1 << l_taskProcessingLogByteSize;
  m_taskPoolSize = 1 << l_taskPoolLogSize;

  uint s = l_taskProcessingLogByteSize + l_taskPoolLogSize;
  cout << "Encrypting 2^" << s << " bytes or 2^"  << s + 3 << " bits per run" << endl;

  // each encryption job is decomposed into three operations:
  // * write data to the device
  // * encryption on the device
  // * read back data from the device
  // to track the dependencies between these tasks,
  // we need two times the number of operations as events
  // this number of events is independent from the task pool size
  m_eventSize = 6;
  m_events = new cl_event[m_eventSize];

  // number of times the pool is processed by the GPU (for benchmarking purposes)
  m_numRuns = 1 << 3;

  // the number of block that a GPU thread processes sequentially.
  // because of setup operations (namely, copies of constant data from global
  // memory to shared memory), the optimum is not necessarily 1
  m_blocksPerThread = 1 << 3;

  // input array size. Since the GPU does not appear to support big buffers, even in 
  // host memory, we set it to the pool size.
  m_inputSize = m_taskProcessingByteSize * m_taskPoolSize;
  m_numTasks = m_taskPoolSize * m_numRuns;
  // number of blocks that are encrypted during the execution of a pool task.
  // block size is 16 bytes and num blocks is task size / block size
  m_numBlocksPerTask = m_taskProcessingByteSize / 16;
  // number of threads required by a pool task; used for workgroup definition.
  m_globalThreads = m_numBlocksPerTask / m_blocksPerThread;
  // work item definition
  m_localThreads = 1 << 7;
  // 0 <= group_id(0) < m_globalThreads / m_localThreads

  // local caches (i.e. buffers copied to thread shared memory) sizes.
  // one local buffer for combined mixColumns - subBytes table,
  // one for the SBox table, one for round keys, of size 'expandedKeySize'
  m_mixColumnBufferSize = 1024 * sizeof(cl_uchar4);
  m_sboxBufferSize = 256 * sizeof(cl_uchar);
}

AESEncrypt::~AESEncrypt()
{
  delete [] m_key;
  delete [] m_expandedKey;
  delete [] m_roundKey;
  delete [] m_verificationOutput;
}

void AESEncrypt::cleanupCL()
{
  try
  {
    if(g_usePinnedMemory)
    { 
      CLManager::checkOutput(clReleaseMemObject(m_inputBuffer), "clReleaseMemObject failed.");
      CLManager::checkOutput(clReleaseMemObject(m_outputBuffer), "clReleaseMemObject failed.");
    }
    else
    {
      delete [] m_input;
      delete [] m_output;
    }
    CLManager::checkOutput(clReleaseMemObject(cmDevBufInOut), "clReleaseMemObject failed.");
    CLManager::checkOutput(clReleaseMemObject(cmDevRoundKeys), "clReleaseMemObject failed.");
    CLManager::checkOutput(clReleaseMemObject(cmDevMixCol), "clReleaseMemObject failed.");
    CLManager::checkOutput(clReleaseMemObject(cmDevSBox), "clReleaseMemObject failed.");
  }
  catch(exception & e)
  {
    cout << e.what() << endl;
  }
  delete [] m_events;
  m_events = 0;
}

//======================================= AES-related functions ======================================

inline void AESEncrypt::rotate(cl_uchar * word)
{
  cl_uchar c = word[0];
  for(cl_uint i = 0; i < 3; ++i)
  {
    word[i] = word[i + 1];
  }
  word[3] = c;
}

void AESEncrypt::keyExpansion(cl_uchar * key, cl_uchar * expandedKey,
    cl_uint keySize, cl_uint expandedKeySize)
{
  cl_uint rConIteration = 1;
  cl_uchar temp[4]      = {0};

  for(cl_uint i=0; i < keySize; ++i)
  {
    expandedKey[i] = key[i];
  }

  assert((keySize%4) == 0);
  assert((expandedKeySize%4)==0);

  for(cl_uint i = keySize/4; i < expandedKeySize/4; ++i)
  {
    cl_uint currentSize = 4*i;
    for(cl_uint j = 0; j < 4; ++j)
    {
      temp[j] = expandedKey[(currentSize - 4) + j];
    }

    if(currentSize%keySize == 0)
    {
      rotate(temp);
      for(cl_uint j = 0; j < 4; ++j)
      {
        temp[j] = sbox[temp[j]];
      }    
      temp[0] = temp[0] ^  Rcon[rConIteration];
      rConIteration++;
    }

    for(cl_uint j = 0; j < 4; ++j)
    {
      expandedKey[currentSize + j] = expandedKey[currentSize + j - keySize]^temp[j];
    }
  }
}

cl_uchar AESEncrypt::galoisMultiplication(cl_uchar a, cl_uchar b)
{
  cl_uchar p = 0; 
  for(cl_uint i=0; i < 8; ++i)
  {
    if((b&1) == 1)
    {
      p^=a;
    }
    cl_uchar hiBitSet = (a & 0x80);
    a <<= 1;
    if(hiBitSet == 0x80)
    {
      a ^= 0x1b;
    }
    b >>= 1;
  }
  return p;
}

void AESEncrypt::createRoundKey(cl_uchar * eKey, cl_uchar * rKey)
{
  for(cl_uint i=0; i < 4; ++i)
    for(cl_uint j=0; j < 4; ++j)
    {
      rKey[i+ j*4] = eKey[i*4 + j];
    }
}

//================================ end of AES-related functions ======================================

