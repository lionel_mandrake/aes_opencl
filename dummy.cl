/***
* OpenCL dummy kernel; copies its input to its output
* Can be used to measure the memory throughput of an OpenCL device.
* Copyright (c) 2014 drs.mandrake@gmail.com
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
* This file is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*/

__kernel void AESEncrypt(
__global  uchar16  * input,
__global  uchar16  * output,
const 	  uint     ioOffset,
const uint blocksPerThread)
{
  const unsigned int localSize = get_local_size(0);
	const unsigned int threadIdx =  get_group_id(0) * localSize * blocksPerThread + get_local_id(0);
	for(unsigned int blk = 0; blk < blocksPerThread; blk++)
	{
  	const unsigned int globalIndex = ioOffset + blk * localSize + threadIdx;
    output[globalIndex] = input[globalIndex];
  }
}

