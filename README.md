AES implementation in OpenCL, under Linux.

Needs a working OpenCL installation and OpenSSL for test and benchmarking purposes. Cmake, which is used as a build tool, and a complete gcc toolchain are needed also.

A custom Cmake script is used for OpenCL detection (CMakeModules/FindOpenCL.cmake). See that file to define the required environment variables (if any) needed for the detection to work. At the moment this is needed for Nvidia hardware.

This software was tested on Ubuntu 14.04 x64 with Intel and AMD OpenCL devices. On such a platform, once in the source dir, the program can be build as follows

```
#!bash


mkdir build_dir
cd build_dir
cmake ..
make
```

The cmake step should not report a missing tool or dependency.

Any comment or request should be directed to drs.mandrake@gmail.com.