/*******************************************************************************
  This file is copyright (c) 2010 SeQureNet SARL
  All rights reserved
 *******************************************************************************/

#pragma once

#ifdef _MSC_VER
#pragma warning(disable:4127 4146)
// disable C4127, "conditional expression is constant"
// and C4146, "unary minus operator applied to unsigned type, result still unsigned"
#endif

// not needed anymore for VC 2010 compiler
//#ifdef _MSC_VER
//#include "pstdint.h"
//#else
#include "stdint.h"
//#endif

#if defined(linux) || defined(__CYGWIN__)
#define _LINUX // for SimpleSockets, which expects this symbol
#endif

#include "stddef.h"

typedef enum _channelType
{
  awgn, bsc, groupGauss,erasure
} channelType;

typedef enum _decodeType
{
  flood, shuffle
} decodeType;

typedef enum _fileFormat
{
  alist, iotech
} fileFormat;

typedef enum _decodingMethod
{
  ldpc, multilevel, polar
} decodingMethod;