
#pragma once


#include <string>
#include <exception>


class GenericException : public std::exception
{
  std::string m_message;
public:

  GenericException(const char* p_message) :
  m_message(p_message)
  {
  }

  GenericException(const std::string & p_message) :
  m_message(p_message)
  {
  }

  ~GenericException() throw ()
  {
  }

  virtual const char* what() const throw ()
  {
    return m_message.c_str();
  }
};

class InvalidMessageException{};


