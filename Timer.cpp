/**
 * Timer class to measure execution times. Me only measure real time, not CPU time.
 *
 * Copyright (c) 2014 drs.mandrake@gmail.com
 *
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#include "Timer.h"

#include <unistd.h>

Timer::Timer(bool p_start) :
m_isStarted(false),
m_totalTime(0.)
{
  if (p_start)
  {
    restart();
  }
}

double Timer::elapsedTime()
{
  double l_elapsedTime = 0.;
  if (m_isStarted)
  {

    gettimeofday(&m_timeEnd, 0);
    l_elapsedTime =
        double(m_timeEnd.tv_sec - m_timeStart.tv_sec) +
        double(m_timeEnd.tv_usec - m_timeStart.tv_usec) * 1e-6;
  }
  return l_elapsedTime;
}

// stops the timer and returns the total elapsed time, in seconds
double Timer::stop()
{
  double l_elapsedTime = elapsedTime();
  m_isStarted = false;
  m_totalTime += l_elapsedTime;
  return m_totalTime;
}

// starts or restarts the timer
void Timer::restart()
{
  if (!m_isStarted)
  {
    gettimeofday(&m_timeStart, 0);
    m_isStarted = true;
  }
}

