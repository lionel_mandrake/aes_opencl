# Copyright (c) 2014 drs.mandrake@gmail.com
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

#this files requires the following environment variables:
# ATISTREAMSDKROOT/AMDAPPSDKROOT for the root of the AMD SDK
# (if installed, normally set by an install of the AMD STREAM SDK)
# NVIDIA_OPENCL for the root of the NVIDIA CUDA toolkit
# (if installed, not set by the toolkit install; usually /usr)
# env vars can be set system-wide in in /etc/environment on debian systems;
# or on a per-user basis in .profile or .bash_profile .

include(FindPackageHandleStandardArgs)

if(DEFINED ENV{ATISTREAMSDKROOT})
  set(ENV_AMD $ENV{ATISTREAMSDKROOT})
else()
  set(ENV_AMD $ENV{AMDAPPSDKROOT})
endif()  

set(ENV_NVIDIA_OPENCL $ENV{NVIDIA_OPENCL})
set(ENV_INTEL_OPENCL $ENV{INTEL_OPENCL})

set(OPENCL_AMD_FOUND FALSE)
set(OPENCL_NVIDIA_FOUND FALSE)
set(OPENCL_INTEL_FOUND FALSE)
set(OPENCL_UNKNOWN_FOUND FALSE)
set(OPENCL_FOUND FALSE)

if(ENV_AMD)
  message("Detected AMD OpenCL implementation")
  set(ENV_OPENCL_DIR_AMD ${ENV_AMD})
  set(ENV_OPENCL_DIR ${ENV_OPENCL_DIR_AMD})
  set(OPENCL_AMD_FOUND TRUE)
endif()

if(ENV_NVIDIA_OPENCL)
  message("Detected NVIDIA OpenCL implementation")
  set(ENV_OPENCL_DIR_NVIDIA ${ENV_NVIDIA_OPENCL})
  set(ENV_OPENCL_DIR ${ENV_OPENCL_DIR_NVIDIA})
  set(OPENCL_NVIDIA_FOUND TRUE)
endif()

if(ENV_INTEL_OPENCL)
  message("Detected Intel OpenCL implementation in ${ENV_INTEL_OPENCL}")
  set(ENV_OPENCL_DIR_INTEL ${ENV_INTEL_OPENCL})
  set(ENV_OPENCL_DIR ${ENV_OPENCL_DIR_INTEL})
  set(OPENCL_INTEL_FOUND TRUE)
endif()
  
if((NOT OPENCL_AMD_FOUND) AND (NOT OPENCL_NVIDIA_FOUND) AND (NOT OPENCL_INTEL_FOUND))
  message("No OpenCL implementation detected through environment variables")
endif()

if(OPENCL_AMD_FOUND  AND (((NOT OPENCL_NVIDIA_FOUND) AND (NOT OPENCL_INTEL_FOUND)) OR PREFER_OPENCL_AMD))
  message("Choosing AMD implementation")
  set(ENV_OPENCL_DIR ${ENV_OPENCL_DIR_AMD})
  set(OPENCL_NVIDIA_FOUND FALSE)
  set(OPENCL_INTEL_FOUND FALSE)
endif()

if(OPENCL_NVIDIA_FOUND AND (((NOT OPENCL_AMD_FOUND) AND (NOT OPENCL_INTEL_FOUND)) OR PREFER_OPENCL_NVIDIA))
  message("Choosing NVIDIA implementation")
  set(ENV_OPENCL_DIR ${ENV_OPENCL_DIR_NVIDIA})
  set(OPENCL_AMD_FOUND FALSE)
  set(OPENCL_INTEL_FOUND FALSE)
endif()

if(OPENCL_INTEL_FOUND AND (((NOT OPENCL_AMD_FOUND) AND (NOT OPENCL_NVIDIA_FOUND)) OR PREFER_OPENCL_INTEL))
  message("Choosing Intel implementation")
  set(ENV_OPENCL_DIR ${ENV_OPENCL_DIR_INTEL})
  set(OPENCL_AMD_FOUND FALSE)
  set(OPENCL_NVIDIA_FOUND FALSE)
endif()

if(NOT(ENV_OPENCL_DIR))
  set(ENV_OPENCL_DIR "/usr")
endif()

find_path(
  OPENCL_INCLUDE_DIR
  NAMES CL/cl.h
  PATHS "${ENV_OPENCL_DIR}/include" "/usr/include" "/usr/local/include" "/usr/local/cuda/include"
  NO_DEFAULT_PATH
  )
#on linux, AMD Stream SDK has its libs in  ${ENV_OPENCL_DIR}/lib/x86 and ${ENV_OPENCL_DIR}/lib/x86_64
# NVIDIA has its libs in ${ENV_OPENCL_DIR}/lib/ and ${ENV_OPENCL_DIR}/lib32/ (64-bit only)

set(LIBPATH "${ENV_OPENCL_DIR}/lib" "${ENV_OPENCL_DIR}/lib/${CMAKE_SYSTEM_PROCESSOR}" "${ENV_OPENCL_DIR}/lib/${CMAKE_SYSTEM_PROCESSOR}-linux-gnu" "${ENV_OPENCL_DIR}/lib/Win32" "${ENV_OPENCL_DIR}/lib/x86" "/usr/lib64")

message("Libraries search path: ${LIBPATH}")

find_library(
  OPENCL_LIBRARIES
  NAMES OpenCL
  PATHS ${LIBPATH}
  NO_DEFAULT_PATH
  )
  message("OpenCL libs: ${OPENCL_LIBRARIES}")

find_package_handle_standard_args(
  OPENCL
  DEFAULT_MSG
  OPENCL_LIBRARIES OPENCL_INCLUDE_DIR
  )
  
if(OPENCL_FOUND)
  if((NOT OPENCL_AMD_FOUND) AND (NOT OPENCL_NVIDIA_FOUND) AND (NOT OPENCL_INTEL_FOUND))
    set(OPENCL_UNKNOWN_FOUND true)
  endif()
else()
	if(OpenCL_FIND_REQUIRED)
		message(SEND_ERROR "Unable to find the required OpenCL libraries\n")
	endif()
  set(OPENCL_AMD_FOUND FALSE)
  set(OPENCL_NVIDIA_FOUND FALSE)
  set(OPENCL_INTEL_FOUND FALSE)
  set(OPENCL_UNKNOWN_FOUND FALSE)
  set(OPENCL_LIBRARIES)
  set(OPENCL_INCLUDE_DIR)
endif()

mark_as_advanced(
  OPENCL_LIB_SEARCH_PATH
  OPENCL_INCLUDE_DIR
  OPENCL_LIBRARIES
  )
